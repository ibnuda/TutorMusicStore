﻿module TutorMusicStore.Albums

open System
open System.Globalization
open System.IO

open Arachne.Http

open Chiron

open Freya.Core
open Freya.Router
open Freya.Machine
open Freya.Machine.Extensions.Http
open Freya.Lenses.Http

open Microsoft.AspNet.Identity

let isMalformed =
  freya {
    let! meth = Freya.Lens.get Request.Method_
    match meth with
    | POST -> let! album = readAlbum in return album.IsNone
    | _ -> return false
  }

let createAlbum =
  freya {
    let! album = readAlbum
    let album = album.Value
    let ctx = Db.getContext ()
    let album =
      Db.createAlbum (
        album.ArtistId,
        album.GenreId,
        album.Price,
        album.Title,
        album.AlbumArtUrl )
        ctx 
    let details = Db.getAlbumDetail album.AlbumId ctx
    return Album.AlbumDetails.FromDb details.Value
  } |> Freya.memo

let post =
  freya {
    let! _ = createAlbum
    return ()
  }

let onCreated _ =
  freya {
    let! album = createAlbum
    do! Freya.Lens.setPartial
          Response.Headers.Location_
          (Location.parse (String.Format (Uris.endpoint + Uris.album, album.AlbumId)))
    return! writeHtml ("album", album)
  }

let fetch: Freya<Db.DbContext -> _> =
  Db.getAlbumDetails >> Array.map Album.AlbumDetails.FromDb |> Freya.init

let pipe =
  freyaMachine {
    methodsSupported (Freya.init [GET; POST])
    malformed isMalformed
    including (protectAuthenticated [GET; POST] (Freya.init Uris.albums))
    including (protectAdmin [GET; POST])
    doPost post
    handleCreated onCreated
  } |> FreyaMachine.toPipeline
