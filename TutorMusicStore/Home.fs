﻿module TutorMusicStore.Home

open Arachne.Http
open Chiron

open Freya.Core
open Freya.Machine
open Freya.Machine.Extensions.Http

type Container =
  { Greeting: string }
  static member ToJson (c: Container) =
    Json.write "greeting" c.Greeting

let fetch = Freya.init (fun (_: Db.DbContext) -> {Greeting = "Wew, lad!"})

let pipe =
  freyaMachine {
    including (res fetch "home")
    methodsSupported (Freya.init [GET])
  } |> FreyaMachine.toPipeline

