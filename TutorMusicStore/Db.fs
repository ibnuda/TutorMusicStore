﻿[<RequireQualifiedAccess>]
module Db

open System

open FSharp.Data.Sql

type Sql =
  SqlDataProvider<
    ConnectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SuaveMusicStore;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False",
    DatabaseVendor=Common.DatabaseProviderTypes.MSSQLSERVER>

type DbContext = Sql.dataContext
type Album = DbContext.``dbo.AlbumsEntity``
type Artist = DbContext.``dbo.ArtistsEntity``
type Genre = DbContext.``dbo.GenresEntity``
type AlbumDetails = DbContext.``dbo.AlbumDetailsEntity``
type User = DbContext.``dbo.UsersEntity``
type Cart = DbContext.``dbo.CartsEntity``
type CartDetails = DbContext.``dbo.CartDetailsEntity``

let getContext () = Sql.GetDataContext ()

let firstOrNone (s: seq<'a>): 'a option = s |> Seq.tryFind (fun _ -> true)

let getGenres (ctx: DbContext): Genre [] =
  ctx.Dbo.Genres |> Seq.toArray

let getArtists (ctx: DbContext): Artist [] =
  ctx.Dbo.Artists |> Seq.toArray

let getAlbum (id: int) (ctx: DbContext): Album option =
  query {
    for album in ctx.Dbo.Albums do
    where (album.AlbumId = id)
    select album
  } |> firstOrNone

let getAlbumForGenre genreName (ctx: DbContext): Album [] =
  query {
    for album in ctx.Dbo.Albums do
    join genre in ctx.Dbo.Genres on (album.GenreId = genre.GenreId)
    where (genre.Name = genreName)
    select album
  } |> Seq.toArray

let getAlbumDetail (id: int) (ctx: DbContext): AlbumDetails option =
  query {
    for album in ctx.Dbo.AlbumDetails do
    where (album.AlbumId = id)
    select album 
  } |> firstOrNone

let getAlbumDetails (ctx: DbContext): AlbumDetails [] =
  ctx.Dbo.AlbumDetails |> Seq.toArray

let createAlbum (artistId, genreId, price, title, albumArtUrl) (ctx: DbContext): Album =
  let album = ctx.Dbo.Albums.Create (artistId, genreId, price, title)
  album.AlbumArtUrl <- albumArtUrl
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()
  album

let newUser (username, password, email) (ctx: DbContext): User =
  let user = ctx.Dbo.Users.Create (email, password, "user", username)
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()
  user

let validateUser (username, password) (ctx: DbContext): User option =
  query {
    for user in ctx.Dbo.Users do
    where (user.UserName = username && user.Password = password)
    select user
  } |> firstOrNone

let getCart cartId albumId (ctx: DbContext): Cart option =
  query {
    for cart in ctx.Dbo.Carts do
    where (cart.CartId = cartId && cart.AlbumId = albumId)
    select cart
  } |> firstOrNone

let getCarts cartId (ctx: DbContext): Cart list =
  query {
    for cart in ctx.Dbo.Carts do
    where (cart.CartId = cartId)
    select cart
  } |> Seq.toList

let getCartDetails cartId (ctx: DbContext): CartDetails list =
  query {
    for cart in ctx.Dbo.CartDetails do
    where (cart.CartId = cartId)
    select cart
  } |> Seq.toList

let addToCart cartId albumId (ctx: DbContext) =
  match getCart cartId albumId ctx with
  | Some cart -> cart.Count <- cart.Count + 1
  | None ->
    ctx.Dbo.Carts.Create (albumId, cartId, 1, DateTime.UtcNow) |> ignore
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()

let removeFromCart (cart: Cart) albumId (ctx: DbContext) =
  cart.Count <- cart.Count - 1
  if cart.Count < 1 then cart.Delete ()
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()

let upgradeCarts (cartId: string, username: string) (ctx: DbContext) =
  for cart in getCarts cartId ctx do
    match getCart username cart.AlbumId ctx with
    | Some existing ->
      existing.Count <- existing.Count + cart.Count
      cart.Delete ()
    | None -> cart.CartId <- username
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()

let placeOrder (username: string) (ctx: DbContext) =
  let carts = getCartDetails username ctx
  let total = carts |> List.sumBy (fun c -> (decimal) c.Count * c.Price)
  let order = ctx.Dbo.Orders.Create (DateTime.UtcNow, total)
  order.Username <- username
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()
  for cart in carts do
    let orderDetails = ctx.Dbo.OrderDetails.Create (cart.AlbumId, order.OrderId, cart.Count, cart.Price)
    getCart cart.CartId cart.AlbumId ctx
    |> Option.iter (fun cart -> cart.Delete ())
  // ctx.SubmitUpdatesAsync () |> Async.StartAsTask |> ignore
  ctx.SubmitUpdates ()
