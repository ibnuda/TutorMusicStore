﻿module TutorMusicStore.Sessions

open System
open System.Security.Claims

open Arachne.Http
open Arachne.Uri.Template

open Freya.Core
open Freya.Core.Operators
open Freya.Lenses.Http
open Freya.Machine
open Freya.Machine.Extensions.Http
open Freya.Machine.Router
open Freya.Router

open Microsoft.AspNet.Identity

let checkCredentials: Freya<Db.User option> =
  freya {
    let! form = form
    return maybe {
      let! username = form |> Map.tryFind "Username"
      let! password = form |> Map.tryFind "Password"
      let ctx = Db.getContext ()
      return! Db.validateUser (username, passHass password) ctx
    }
  } |> Freya.memo

let upgradeCart (username: string): Freya<Unit> =
  freya {
    let! cartId = getRequestCookie "cartId"
    cartId |> Option.iter (fun cartId ->
      let ctx = Db.getContext ()
      Db.upgradeCarts (cartId, username) ctx
    )
  }

let authenticate: Freya<Unit> =
  freya {
    let! user = checkCredentials
    match user with
    | Some user ->
      do! signIn {Username = user.UserName; Role = user.Role}
      do! upgradeCart user.UserName
      do! deleteResponseCookie "cartId"
    | _ -> ()
  }

let seeOther _ =
  freya {
    let! query = query
    let returnPath = defaultArg (Map.tryFind "returnUrl" query) Uris.home

    do! Freya.Lens.setPartial
          Response.Headers.Location_
          (Location.parse (Uris.endpoint + returnPath))

    return
      { Data = [||]
        Description =
          { Charset = None
            Encodings = None
            MediaType = None
            Languages = None } }
  }

let doUnauthorized _ =
  freya {
    let! query = query
    let returnPath = defaultArg (Map.tryFind "returnUrl" query) Uris.home

    return! writeHtml ("logon", {
      Logon.ReturnUrl = returnPath
      Logon.ValidationMsg = "please try again." })
  }

let pipe =
  freyaMachine {
    including common
    methodsSupported (Freya.init [POST])
    authorized (checkCredentials |> Freya.map Option.isSome)
    handleUnauthorized doUnauthorized
    doPost authenticate
    postRedirect (Freya.init true)
    handleSeeOther seeOther
  } |> FreyaMachine.toPipeline