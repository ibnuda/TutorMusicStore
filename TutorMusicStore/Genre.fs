﻿module TutorMusicStore.Genre

open Arachne.Http
open Chiron
open Chiron.Operators

open Freya.Core
open Freya.Machine
open Freya.Machine.Extensions.Http
open Freya.Router

type Genre =
  { Name: string
    Albums: Album.Album [] }

  static member ToJson (g: Genre) =
    Json.write "name" g.Name
    *> Json.write "albums" g.Albums

let name =
  freya {
    let! name = Freya.Lens.getPartial (Route.Atom_ "0")
    return name.Value
  }

let fetch: Freya<Db.DbContext -> _> =
  freya {
    let! name = name
    return
      Db.getAlbumForGenre name
      >> Array.map Album.Album.FromDb
      >> (fun albums -> {Name = name; Albums = albums})
  }

let pipe =
  freyaMachine {
    including (res fetch "genre")
    methodsSupported (Freya.init [GET])
  } |> FreyaMachine.toPipeline



